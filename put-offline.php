<?php

if (!function_exists('sinetiks_offline_header')) :
    function sinetiks_offline_header($status_header, $header, $text, $protocol)
    {
        if (!is_user_logged_in()) {
            return "$protocol 503 Service Unavailable";
        }
    }
endif;

if (!function_exists('sinetiks_offline_content')) :
    function sinetiks_offline_content()
    {
        if (!is_user_logged_in()) {
            $page = "<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<style type='text/css'>
    .container-wrapper{box-shadow:none;}
</style>
</head>
<body style='color:#304050;text-align:center;padding:10%'>
<h1>".get_bloginfo('name')."</h1>
<br />
<a href='/wp-login.php?redirect_to=".htmlspecialchars(home_url())."'>login</a>
</body>
</html>";
            die($page);
        }
    }
endif;

if (!function_exists('sinetiks_offline_feed')) :
    function sinetiks_offline_feed()
    {
        if (!is_user_logged_in()) {
            die('<?xml version="1.0" encoding="UTF-8"?>'.
            '<status>Service unavailable</status>');
        }
    }
endif;

if (!function_exists('sinetiks_add_feed_actions')) :
    function sinetiks_add_feed_actions()
    {
        $feeds = array ('rdf', 'rss', 'rss2', 'atom');
        foreach ($feeds as $feed) {
            add_action('do_feed_'.$feed, 'sinetiks_offline_feed', 1, 1);
        }
    }
endif;

if (function_exists('add_filter')) :
    add_filter('status_header', 'sinetiks_offline_header', 10, 4);
    add_action('get_header', 'sinetiks_offline_content');
    sinetiks_add_feed_actions();
else :
// Prevent direct invocation by user agents.
    die('Get off my lawn!');
endif;
