<?php
/*
Plugin Name: Sinetiks-Offline
Description: Pone el sitio fuera de l&iacute;nea y manda un mensaje '503 Service Unavailable'
Author: Sinetiks.com
Author URI: http://sinetiks.com
*/

/*basado en wet-maintenance-wordpress-plugin*/

/**
 * Widget for dashboard
 */
include __DIR__ . '/dashboard-widget/dashboard-widget.php';

/**
 * if offline_enabled setting is = 1 then put offline
 */

if (get_option('offline_enabled', 0) == 1) {
    include __DIR__ . '/put-offline.php';
}
