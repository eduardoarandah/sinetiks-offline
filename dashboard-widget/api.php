<?php

/**
 * Consultar status
 */
add_action('rest_api_init', function () {
    register_rest_route('app/', 'offline-enabled', [
    'methods'  => 'GET',
    'callback' => function () {
        
        //permissions
        if (!current_user_can('administrator')) {
            header('HTTP/1.0 403 Forbidden');
            die('Forbidden');
        }

        return get_option('offline_enabled', 0);
    }
    ]);
});

/**
 * Guardar status
 */
add_action('rest_api_init', function () {
    register_rest_route('app/', 'offline-enabled', [
    'methods'  => 'POST',
    'callback' => function ($request) {

        //permissions
        if (!current_user_can('administrator')) {
            header('HTTP/1.0 403 Forbidden');
            die('Forbidden');
        }

        if (isset($request['offline_enabled'])) {
            $offline_enabled=intval($request['offline_enabled']);
            update_option("offline_enabled", $offline_enabled);
            return $offline_enabled;
        } else {
            return null;
        }
    }
    ]);
});
