jQuery(document).ready(function ($) {

    function cargarEstado(){
        $.ajax({
                url: settings.endPoint,
                type: 'GET',
                beforeSend: function ( xhr ) {
                    //https://v2.wp-api.org/guide/authentication/
                    xhr.setRequestHeader( 'X-WP-Nonce', settings.nonce );
                },
                success: function (data) {
                    cambiarEstado(data);
                }
            });
    }
    function cambiarEstado(estado){
        if(estado=="1"){
            $('#dashboard-widget-offline-offline').show();
            $('#dashboard-widget-offline-online').hide();
        }
        else{
            $('#dashboard-widget-offline-offline').hide();
            $('#dashboard-widget-offline-online').show();
        }
    }

    $('.dashboard-widget-offline-poner-online').click(function(){           
        $.ajax({
                url: settings.endPoint,
                type: 'POST',
                beforeSend: function ( xhr ) {
                    //https://v2.wp-api.org/guide/authentication/
                    xhr.setRequestHeader( 'X-WP-Nonce', settings.nonce );
                },
                data: {
                    'offline_enabled': 0
                },
                success: function (data) {
                    cargarEstado();
                }
            });
    });

    $('.dashboard-widget-offline-poner-offline').click(function(){
        $.ajax({
                url: settings.endPoint,
                type: 'POST',
                beforeSend: function ( xhr ) {
                    //https://v2.wp-api.org/guide/authentication/
                    xhr.setRequestHeader( 'X-WP-Nonce', settings.nonce );
                },
                data: {
                    'offline_enabled': 1
                },
                success: function (data) {
                    cargarEstado();
                }
            });
    });

    cargarEstado();
});