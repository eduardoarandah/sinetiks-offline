<h3 id="dashboard-widget-offline-offline" style="display: none">
    El sitio está
    <span style="font-weight: bold; color: red">fuera de línea</span>
	<p>
	    <button class="button dashboard-widget-offline-poner-online">poner en línea</button>
	</p>
 </h3>
 <h3 id="dashboard-widget-offline-online" style="display: none">
    El sitio está
    <span style="font-weight: bold; color: green">en línea</span>
	<p>
	    <button class="button dashboard-widget-offline-poner-offline">poner fuera de línea</button>
	</p>
 </h3>