<?php
/**
 * WIDGET QUE USA UN ENDPOINT JSON PARA HACER LOS CAMBIOS
 */
include __DIR__ . '/api.php';


/**
 * Registra widget
 */
add_action('wp_dashboard_setup', function () {
    wp_add_dashboard_widget(
        'dashboard_widget_offline',
        'Poner el sitio fuera de línea',
        function ($post, $callback_args) {
            include __DIR__ . '/render.php';
        }
    );
});

/**
 * Cargar JSON agregando la variable "settings"
 * https://v2.wp-api.org/guide/authentication/
 */
add_action('admin_enqueue_scripts', function () {
    wp_register_script('sinetiks-offline-dashboard', plugins_url('main.js', __FILE__), ['jquery'], "1.0");
    wp_localize_script('sinetiks-offline-dashboard', 'settings', array(
    'endPoint' => esc_url_raw(rest_url())."app/offline-enabled",
    'nonce' => wp_create_nonce('wp_rest')
    ));
    wp_enqueue_script('sinetiks-offline-dashboard');
});
